// Class dat data laad en omzet tot objecten.
// Ik kon op het moment geen goede naam vinden voor het object en heb het daarom Frame genoemd.
class Frame {
	
	public:
		// Omdat er niet is gespecificeerd waar de data vandaan zou komen ben ik er voor het gemak vanuit gegaan dat deze uit een xml/json bestand komen.
		Frame(char filePath);
		
		// Veranderd het te laden agenda bestand en roept ClearEvents en LoadAgenda aan.
		void changeAgenda(char filePath) ;
		
		// Veranderd de focus van het frame naar een nieuwe datum en laat de correcte events zien.
		void viewDate(int day, int month, int year) ;
		
		// Roept de Draw events van de events aan en tekent zelf mogelijk een achtergrond en navigatie.
		void draw() ;
	
	private:
		// Wordt gebruikt om de data uit de gelezen bestanden op te slaan.
		char agendaContent ;
	
		// Wordt gebruikt voor het opslaan voor een vantevoren onbekend aantal events.
		std::vector<Event *> eventList ;
	
	
	
		// Laad een nieuw agenda bestand, en stopt de data in agendaContent.
		void loadAgenda(char filePath) ;
	
		// Verwijderd de bestaande Event objecten.
		void clearEvents() ;
	
		// Maakt nieuwe events aan op basis van de gelezen data.
		void buildEvents(char data) ;
}
