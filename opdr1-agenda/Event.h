// Class dat de informatie van een evenement/gebeurtenis binnen het rooster bevat.
class Event {
	
	public:
		enum EventUrgency { normal, important };
		enum EventType { lecture, exam };
	
		// Omdat er niet is aangegeven waar de data vandaan zou komen en hoe deze word aangeleverd ben ik er hier voor het gemak van uitgegaan dat deze als char binnenkomt.
		// De contructor stuurt de char naar een parse functie (ParseData).
		Event(char data);
		
		// Tekent het evenement
		void draw() ;
	
	private:
		// Word gebruikt om de plaats binnen het rooster te bepalen
		Date eventStartDate ;
		Date eventEndDate ;
	
		// De urgentie van het evenement.
		// Kan gebruikt worden bij het vormgeven van het rooster om de aandacht te trekken naar meer belangrijke events.
		EventUrgency urgency ;
	
		// Event data en bechrijving
		char name, acronym, description ;
	
		// Docent data
		char teacherName, teacherAcronym ;
	

		// Haalt de relevante data uit de char en plaatst deze in de variabelen.
		void parseData(char data) ;
}
