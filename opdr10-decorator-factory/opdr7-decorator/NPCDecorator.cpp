#include "NPCDecorator.h"

#include "FarmerDecorator.h"
#include "SoldierDecorator.h"
#include "ShamanDecorator.h"

#include "Elve.h"
#include "Orc.h"

NPCDecorator::NPCDecorator(NPC * target) {
	this->target = target;
}

NPCDecorator::~NPCDecorator() {
	delete this->target;
}

void NPCDecorator::render() {
	this->target->render();
}

NPC* NPCDecorator::createEntity(std::string name, Species s) {
	switch(s) {
		case ELVE:
			return new Elve(name);
		case ORC:
			return new Orc(name);
	}

	return nullptr;
}

NPC * NPCDecorator::createEntity(std::string name, Species s, Job j) {
	return giveJob(createEntity(name, s), j);
}

NPC * NPCDecorator::giveJob(NPC * base, Job j) {
	switch(j) {
		case FARMER:
			return new FarmerDecorator(base);
		case SOLDIER:
			return new SoldierDecorator(base);
		case SHAMAN:
			return new ShamanDecorator(base);
	}

	return nullptr;
}
