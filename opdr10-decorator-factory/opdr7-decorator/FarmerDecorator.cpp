#include "FarmerDecorator.h"

void FarmerDecorator::render() {
	NPCDecorator::render();
	std::cout << "Farmer" << std::endl;
}
