#pragma once
#include <iostream>
#include "NPCDecorator.h"

class FarmerDecorator : public NPCDecorator {
	public:
	FarmerDecorator(NPC * target) : NPCDecorator(target) {};
	void render();
};

