#pragma once
#include <iostream>
#include "NPCDecorator.h"

class ShamanDecorator : public NPCDecorator {
	public:
	ShamanDecorator(NPC * target) : NPCDecorator(target) {};
	void render();
};

