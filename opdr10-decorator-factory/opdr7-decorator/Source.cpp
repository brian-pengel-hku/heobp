#include <iostream>
#include <string>
#include "NPCDecorator.h"


int main () {
	/*
		ORIGINAL

	NPC * elve = new Elve("Adele");
	elve = new FarmerDecorator(elve);
	elve = new ShamanDecorator(elve);

	NPC * orc = new Orc("Ugluk");
	orc = new FarmerDecorator(orc);
	orc = new SoldierDecorator(orc);
	orc = new ShamanDecorator(orc);

	elve->render();
	orc->render();

	*/

	// ////////////////////////////////////////// //
	// Decorators door middel van static factory
	// ////////////////////////////////////////// //
	NPC * elve = NPCDecorator::createEntity("Adele", ELVE, FARMER);
	NPC * orc = NPCDecorator::createEntity("Ugluk", ORC, SOLDIER);

	// met meerdere jobs
	elve = NPCDecorator::giveJob(elve, SHAMAN);

	elve->render();
	orc->render();

	delete elve;
	delete orc;
	return 0;
}