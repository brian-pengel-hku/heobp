#pragma once
#include <iostream>
#include "NPCDecorator.h"


class SoldierDecorator : public NPCDecorator {
	public:
	SoldierDecorator(NPC * target) : NPCDecorator(target) {};
	void render();
};

