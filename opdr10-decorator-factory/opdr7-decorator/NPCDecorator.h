#include <string>
#include "NPC.h"

enum Species { ELVE, ORC };
enum Job { FARMER, SOLDIER, SHAMAN };

#pragma once
class NPCDecorator : public NPC {
	private:
	NPC * target;

	public:
	NPCDecorator(NPC * target);
	~NPCDecorator();
	void render();

	static NPC * createEntity (std::string name, Species s);
	static NPC * createEntity (std::string name, Species s, Job j);

	static NPC * giveJob (NPC * base, Job j);
	
};

