#include "Elve.h"



Elve::Elve(std::string name) { 
	this->name = name;
}

void Elve::render() {
	std::cout << "-- Elve named: " + this->name << std::endl;
}
