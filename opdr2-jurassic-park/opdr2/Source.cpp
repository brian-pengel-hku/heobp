#include <iostream>
#include "Park.h"

int main() {
	Park jurassicPark = Park();

	jurassicPark.addDinosaur(Dino("Allosaurus"));
	jurassicPark.addDinosaur(Dino("Velociraptor"));
	jurassicPark.addDinosaur(Dino("Megalodon"));
	jurassicPark.addDinosaur(Dino("Tyrannosaurus Rex"));
	jurassicPark.addDinosaur(Dino("Velociraptor"));
	jurassicPark.addDinosaur(Dino("Stegosaurus"));

	jurassicPark.list();
	jurassicPark.removeDinosByName("Velociraptor");
	jurassicPark.list();

	std::cout << "Clever girl" << std::endl;

	return 0;
}