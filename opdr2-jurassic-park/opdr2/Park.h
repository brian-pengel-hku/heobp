#pragma once
#include <vector>
#include <string>
#include "Dino.h"

class Park {
	public:
		Park();
		void addDinosaur(Dino newDino);
		void removeDinosByName(std::string name);
		void list();

	private:
		std::vector<Dino> dinosaurs;
};

