#include "Park.h"

#include <iostream>


Park::Park() {
	
}

void Park::addDinosaur(Dino newDino) {
	dinosaurs.push_back(newDino);
}

void Park::removeDinosByName(std::string name) {
	std::vector<Dino>::iterator i = dinosaurs.begin();

	while (i != dinosaurs.end()) {
		if (i->getName() == name)
			i = dinosaurs.erase(i);
		else
			i++;
	}
}

void Park::list() {
	std::cout << "----- In the park:" << std::endl;
	std::vector<Dino>::iterator it = dinosaurs.begin();
	while (it != dinosaurs.end()) {
		std::cout << it->getName() << std::endl;
		it++;
	}
	std::cout << "-----" << std::endl;
}