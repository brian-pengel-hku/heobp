#include "stdafx.h"
#include "Verkoop.h"


Verkoop::Verkoop() {}

void Verkoop::verwerkBTW(BTWBerekening* berekening) {
	double item1 = 25.50;
	double item2 = 13.75;

	double btw1 = berekening->geefBTW(BTWBerekening::ITEMSOORT_BOEKEN, item1);
	double btw2 = berekening->geefBTW(BTWBerekening::ITEMSOORT_VOEDSEL, item2);

	double totaalBTW = btw1 + btw2;
}
