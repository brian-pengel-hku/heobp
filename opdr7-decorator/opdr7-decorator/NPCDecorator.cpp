#include "NPCDecorator.h"

NPCDecorator::NPCDecorator(NPC * target) {
	this->target = target;
}

NPCDecorator::~NPCDecorator() {
	delete this->target;
}

void NPCDecorator::render() {
	this->target->render();
}
