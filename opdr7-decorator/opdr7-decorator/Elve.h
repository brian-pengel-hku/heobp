#include <iostream>
#include <string>
#include "NPC.h"

#pragma once
class Elve : public NPC {
	public:
	Elve(std::string name);
	void render();
};

