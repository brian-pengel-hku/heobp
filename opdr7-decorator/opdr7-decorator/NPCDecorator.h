#include "NPC.h"

#pragma once
class NPCDecorator : public NPC {
	private:
	NPC * target;

	public:
	NPCDecorator(NPC * target);
	~NPCDecorator();
	void render();
};

