#include <iostream>
#include <string>
#include "Elve.h"
#include "Orc.h"
#include "FarmerDecorator.h"
#include "SoldierDecorator.h"
#include "ShamanDecorator.h"

int main () {
	NPC * elve = new Elve("Adele");
	elve = new FarmerDecorator(elve);
	elve = new ShamanDecorator(elve);

	NPC * orc = new Orc("Ugluk");
	orc = new FarmerDecorator(orc);
	orc = new SoldierDecorator(orc);
	orc = new ShamanDecorator(orc);

	elve->render();
	orc->render();

	delete elve;
	delete orc;
	return 0;
}