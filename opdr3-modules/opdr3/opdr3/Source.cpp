#include <iostream>
#include <vector>

#include "Module.h"
#include "Student.h"


std::vector<Module*> modules;
std::vector<Student*> students;

template <typename T>
void deletePointersInVector(std::vector<T>& v) {
	typename std::vector<T>::iterator i = v.begin();

	while(i != v.end()) {
		delete (*i);
		i = v.erase(i);
	}
}

template <typename T>
void printList(std::string listName, std::vector<T>& v)  {
	std::vector<T>::iterator i = v.begin();

	std::cout << "\n\n--- " + listName << std::endl;

	while(i != v.end()) {
		(*i)->print();
		i++;
	}

	std::cout << "---"<< std::endl;
}

int main() {
	// Maak 3 Modules - Maak 3 docenten
	// Wijs docenten toe aan module
	modules.push_back(new Module("HEOBP", new Docent("Edwin"), 2));
	modules.push_back(new Module("KERGDV1", new Docent("Valentijn"), 4));
	modules.push_back(new Module("KERGDV2", new Docent("Aaron"), 8));

	// Maak 10 studenten
	for(int cnt = 0; cnt < 10; ++cnt) {
		Student* student = new Student("Student" + std::to_string(cnt));
		students.push_back(student);

		// Wijs studenten 1-3 modules toe
		for(size_t i = 0; i < (rand() % modules.size() + 1); ++i) {
			(*student).assignModule(modules.at(i));
		}
	}

	// Print een lijst van modules, met docent & modules
	printList("Module list", modules);

	// Toon totaal EC per student
	printList("Student list", students);

	// Wijzig EC van 1 module
	modules[1]->setEC(12);
	printList("Module list", modules);

	// Toon totaal EC per student
	printList("Student list", students);

	// Verwijder student uit module
	students[0]->removeAssignment(students[0]->getAssignedModules()[0]);

	// & toon lijst opnieuw
	printList("Student list", students);


	deletePointersInVector(modules);
	deletePointersInVector(students);

	modules.clear();
	students.clear();

	return 0;
}