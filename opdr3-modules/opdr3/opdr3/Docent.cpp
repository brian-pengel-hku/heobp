#include "Docent.h"



Docent::Docent(std::string name){
	this->name = name;
}

std::string Docent::getName() {
	return name;
}


Docent::~Docent() {
	std::cout << "Destructor Teacher: " + name << std::endl;
}
