#include "Student.h"



Student::Student(std::string name) {
	this->name = name;
}

int Student::getEC() {
	int res = 0;
	std::vector<Module*>::iterator i = assignedModules.begin();

	while(i != assignedModules.end()) {
		res += (*i)->getEC();
		i++;
	}

	return res;
}

void Student::assignModule(Module* module) {
	assignedModules.push_back(module);
}

std::vector<Module*> Student::getAssignedModules() {
	return assignedModules;
}

void Student::removeAssignment(Module* module){
	size_t pos = find(assignedModules.begin(), assignedModules.end(), module) - assignedModules.begin();
	assignedModules.erase(assignedModules.begin() + pos);
}

void Student::print() {
	std::cout << "\t Student: " + name + "\t Totaal EC:" + std::to_string(getEC()) + "\t" + getAssignedModulesAsString() << std::endl;
}

Student::~Student() {
	std::cout << "Destructor " + name << std::endl;
}

std::string Student::getAssignedModulesAsString() {
	std::string str = "";

	std::vector<Module*>::iterator i = assignedModules.begin();
	while(i != assignedModules.end()) {
		str += (*i)->getName() + ", ";
		i++;
	}

	return str;
}
