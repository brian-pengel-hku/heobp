#pragma once
#include <string>
#include <vector>
#include <iostream>
#include "Module.h"

class Student {
	public:
		Student(std::string name);
		int getEC();
		void assignModule(Module* module);
		std::vector<Module*> getAssignedModules();
		void removeAssignment(Module* module);
		void print();
		~Student();

	private:
		std::string name;
		std::vector<Module*> assignedModules;

		std::string getAssignedModulesAsString();

};

