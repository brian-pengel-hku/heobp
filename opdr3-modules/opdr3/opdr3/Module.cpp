#include "Module.h"



Module::Module(std::string name, Docent* teacher, int ec){
	this->name = name;
	this->teacher = teacher;
	this->ec = ec;
}

void Module::print() {
	std::cout << "\t Module: " + name + "\t Docent:" + teacher->getName() + "\t EC:" + std::to_string(getEC()) << std::endl;
}

int Module::getEC() {
	return ec;
}

void Module::setEC(int ec) {
	this->ec = ec;
}

std::string Module::getName() {
	return name;
}


Module::~Module() {
	std::cout << "Destructor module: " + name << std::endl;
	delete teacher;
}
