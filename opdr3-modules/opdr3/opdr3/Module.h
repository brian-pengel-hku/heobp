#pragma once
#include <string>
#include <iostream>

#include "Docent.h"

class Module {
	public:
		Module(std::string name, Docent* teacher, int ec);

		void print();
		int getEC();
		void setEC(int ec);

		std::string getName();

		~Module();

	private:
		std::string name;
		Docent* teacher;
		int ec;
};

