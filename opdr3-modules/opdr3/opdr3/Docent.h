#pragma once
#include <string>
#include <iostream>

class Docent {
	public:
		Docent(std::string name);
		std::string getName();
		~Docent();

	private:
		std::string name;
};

