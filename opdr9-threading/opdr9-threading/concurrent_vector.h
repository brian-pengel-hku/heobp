#include <vector>
#include <mutex>

#pragma once

template <typename T>
class concurrent_vector {
	private:
	bool writeLock = false ;
	std::condition_variable writeCondition;

	std::vector<T> internalVec;

	public:
	concurrent_vector();

	void push_back (T i);
	void writeQueue ();
	void writeReserve(bool b);

	typename std::vector<T>::iterator begin ();
	typename std::vector<T>::iterator end ();


};

template <typename T>
concurrent_vector<T>::concurrent_vector() {
	internalVec = std::vector<T> ();
}

template <typename T>
void concurrent_vector<T>::push_back(T i) {
	mutex.lock();
	internalVec.push_back(i);
	mutex.unlock();
}

template<typename T>
inline void concurrent_vector<T>::writeQueue() {
	std::unique_lock<std::mutex> lock(mutex);

	while(writeLock)
		writeCondition.wait(lock);
}

template<typename T>
inline void concurrent_vector<T>::writeReserve(bool b) {
	writeLock = b;
	if(!b) writeCondition.notify_one();
}

template <typename T>
inline typename std::vector<T>::iterator concurrent_vector<T>::begin() {
	return internalVec.begin();
}

template <typename T>
inline typename std::vector<T>::iterator concurrent_vector<T>::end() {
	return internalVec.end();
}
