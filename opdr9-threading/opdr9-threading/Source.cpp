#include <queue>
#include <thread>
#include <iostream>
#include "concurrent_vector.h"

// //////// //
// Opdr 1
// //////// //
void PushToVec (concurrent_vector<int> * vec, int size) {
	vec->writeQueue();
	vec->writeReserve(true);

	int i = 0;
	while(i < size) {
		vec->push_back(i);
		i++;
	}

	vec->writeReserve(false);
}

bool CheckVector (concurrent_vector<int> * vec, int size) {
	std::vector<int>::iterator it = vec->begin();
	int i = 0;

	while(it != vec->end()) {
		if((*it) != i) {
			std::cout << (*it) << ", " << i << std::endl;
			return false;
		}

		//std::cout << (*it) << ", " << i << std::endl;

		it++;
		i++;
		i = i % size;
	}

	return true;
}

// //////// //
// Opdr 2
// //////// //
std::condition_variable flag;
std::mutex mutex;

int c = 0;
bool done = false;
std::queue<int> goods;

void producer() {
	for(int i = 0; i < 500; ++i) {
		goods.push(i);
		c++;
	}

	done = true;
	flag.notify_one();
}

void consumer() {
	std::unique_lock<std::mutex> lock(mutex);

	while(!done) {
		flag.wait(lock);

		while(!goods.empty()) {
			goods.pop();
			c--;
		}
	}
}

int main (){
	// //////// //
	// Opdr 1
	// //////// //
	std::cout << std::endl << "--- Opdr 1 ---" << std::endl;
	std::thread producerThread(producer);
	std::thread consumerThread(consumer);

	producerThread.join();
	consumerThread.join();

	std::cout << "Net: " << c << std::endl;


	// //////// //
	// Opdr 2
	// //////// //
	std::cout << std::endl << "--- Opdr 2 ---" << std::endl;
	const int checkSize = 10000;
	concurrent_vector<int>* vec = new concurrent_vector<int>();

	std::thread thread1 (PushToVec, vec, checkSize);
	std::thread thread2 (PushToVec, vec, checkSize);
	std::thread thread3 (PushToVec, vec, checkSize);
	std::thread thread4 (PushToVec, vec, checkSize);
	std::thread thread5 (PushToVec, vec, checkSize);
	thread1.join();
	thread2.join();
	thread3.join();
	thread4.join();
	thread5.join();

	std::cout << (CheckVector (vec, checkSize) ? "No race condition" : "Race condition") << std::endl;

	delete vec;
	
	return 0;
}
