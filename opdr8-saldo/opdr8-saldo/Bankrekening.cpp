#include "Bankrekening.h"
#include <iostream>

Bankrekening::Bankrekening(double saldo) {
	transactions = std::vector<Transactie>();
	this->saldo = saldo;
}


Bankrekening& Bankrekening::operator+(Transactie& t) {
	saldo += t.getAmount() * t.getSign() ;
	transactions.push_back(t);
	return *this;
}

Bankrekening& Bankrekening::operator+=(Transactie& t) {
	return *this + t;
}

Bankrekening& Bankrekening::operator-(Transactie& t) {
	t.setSign(-1);
	return *this + t;
}

Bankrekening& Bankrekening::operator-=(Transactie& t) {
	return *this - t;
}


std::ostream & operator<<(std::ostream & s, Bankrekening r) {
	std::vector<Transactie> tr = r.transactions;

	s << "---Rekening---\nsaldo: " << r.saldo << "\n\n";
	for(size_t i = 0; i < tr.size(); i++) {
		s << "> " << tr[i].toString() << std::endl ;
	}
	return s;
}
