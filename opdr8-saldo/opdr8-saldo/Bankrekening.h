#include <vector>
#include "Transactie.h"

#pragma once
class Bankrekening {
	std::vector<Transactie> transactions;
	double saldo;

	public:
	Bankrekening(double saldo);

	Bankrekening & operator+(Transactie& t);
	Bankrekening & operator+=(Transactie & t);
	Bankrekening & operator-(Transactie& t);
	Bankrekening & operator-=(Transactie & t);

	friend std::ostream & operator<<(std::ostream & s, const Bankrekening r);

};
