#include <iostream>
#include "Bankrekening.h"
#include "Transactie.h"

int main () {
	Bankrekening rekening = Bankrekening(0);
	rekening += Transactie(500);
	rekening -= Transactie(20);
	rekening += Transactie(50);
	rekening += Transactie(-30);

	std::cout << rekening << std::endl;

	return 0;
}