#include <string>
#include <sstream>
#include <ctime>

#pragma once
class Transactie {
	private:
	double amount;
	int sign = 1;
	std::string date;

	public:
	Transactie(double amount);
	double getAmount();
	void setSign(int a);
	int getSign();
	std::string toString();

	template<typename T>
	static int sgn(T val) {
		return (T(0) < val) - (val < T(0));
	}
};

