#include "Transactie.h"

std::string Transactie::toString() {
	std::ostringstream strs ;
	strs << date << " | " << amount << "\t" << (sign == -1 ? "-" : "+");
	return strs.str();
}

Transactie::Transactie(double amount) {
	sign = sgn<double>(amount);
	this->amount = amount * sign ;
	
	time_t t = time(0);
	struct tm * now = localtime(&t);
	std::ostringstream strs;

	strs << now->tm_mday << "-" << (now->tm_mon + 1) << "-" << (now->tm_year + 1900);

	date = strs.str();
}

double Transactie::getAmount() {
	return amount;
}

void Transactie::setSign(int a) {
	sign = sgn<int>(a);
}

int Transactie::getSign() {
	return sign;
}
