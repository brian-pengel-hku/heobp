#include "Grid.h"



Grid::Grid(int width, int height) {
	this->width = width;
	this->height = height;
	ClearField();
	populate();
}


Grid::~Grid() {
	decimatePopulation();
	std::cout << "Grid destroyed" << std::endl;
}

int Grid::getWidth() {
	return this->width;
}

int Grid::getActivePopulation() {
	return activeGeneration.size();
}

void Grid::populate() {
	for(int cnt = 0; cnt < this->width* this->height; cnt++) {
		Cell * newCell = new Cell(this, cnt);
		cells.push_back(newCell);
		if(newCell->isAlive())
			activeGeneration.push_back(newCell);
	}
}

void Grid::decimatePopulation() {
	auto i = cells.begin();
	while(i != cells.end()) {
		delete (*i);
		i = cells.erase(i);
	}
}

void Grid::liveGeneration() {
	auto i = activeGeneration.begin();

	while(i != activeGeneration.end()) {
		(*i)->live();
		possibleGeneration.push_back(*i);
		i++;
	}

	activeGeneration.clear();
	evaluatePossibleCells();
}

void Grid::evaluatePossibleCells() {
	auto i = possibleGeneration.begin();

	while(i != possibleGeneration.end()) {
		if((*i)->shouldBeAlive()) {
			DrawCell(*i);
			activeGeneration.push_back(*i);
		}
		else
			(*i)->reset();
		i = possibleGeneration.erase(i);
	}
}

bool Grid::isCell(int x, int y) {
	return x >= 0 && x < width && y >= 0 && y < height;
}

Cell* Grid::getCell(int x, int y){
	return cells.at(x + y * width);
}

void Grid::addPossibleActiveCell(Cell* possible) {
	possibleGeneration.push_back(possible);
}


// Draw
void Grid::DrawField() {
	std::string line = std::string(width, '-');
	std::cout << line << gridVisualisation << std::endl << line ;
}

void Grid::DrawCell(Cell * cell) {
	gridVisualisation.replace(cell->getPosition(), 1, cell->getLiveNeighbourCount());
}

void Grid::ClearField() {
	gridVisualisation = std::string(width * height, inactiveChar);
}

void Grid::CreateFieldRows() {
	for(int cnt = 0; cnt < height; cnt++)
		gridVisualisation.insert(width * cnt + cnt, 1, '\n');
}

// Test
void Grid::injectLife (int positions[], int size) {
	for(int cnt = 0; cnt < size; cnt++)
		injectLife(positions[cnt]);
}

void Grid::injectLife (int position) {
	Cell* cur = cells.at(position);
	activeGeneration.push_back(cur);
	cur->setAlive(true);
	DrawCell(cur);
}

void Grid::addOccilator(int pos) {
	int arr[] = {
		pos,
		pos + width,
		pos + width * 2
	};

	injectLife(arr, 3);
}

void Grid::addStill(int pos) {
	int arr[] = {
		pos,
		pos + 1,
		pos + width
	};

	injectLife(arr, 3);
}

void Grid::randomise() {
	auto i = cells.begin();

	do {
		if(rand() % 2 == 1)
			injectLife((*i)->getPosition());
	} while(i++, i != cells.end());
}