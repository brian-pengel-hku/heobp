#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Grid.h"

class Grid;
class Cell {

	Grid* grid;
	bool alive;
	std::vector<Cell*> neighbours;
	int position = 0, 
		x = 0, 
		y = 0, 
		activeNeighbours = 0;

	void saveNeighbours ();

	public:
	Cell(Grid* grid, int position);
	~Cell();

	void live ();
	bool shouldBeAlive();
	bool isAlive ();
	void setAlive (bool alive);
	void reset ();

	int getYPosition ();
	int getXPosition ();
	int getPosition ();

	std::string getLiveNeighbourCount();
	void knock ();
};
