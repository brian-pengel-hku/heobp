#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Cell.h"

class Cell;
class Grid {
	int width,
		height;

	const char inactiveChar = ' '; // '-';
	std::string gridVisualisation;
	std::vector<Cell*> cells, activeGeneration, possibleGeneration;

	void populate();
	void decimatePopulation();

	public:
	Grid(int width, int height);
	~Grid();

	int getWidth();
	int getActivePopulation();
	Cell* getCell (int x, int y);
	bool isCell (int x, int y);

	void liveGeneration();
	void evaluatePossibleCells();
	void addPossibleActiveCell (Cell* possible);

	// Draw
	void DrawField();
	void DrawCell(Cell* cell);
	void ClearField();
	void CreateFieldRows();

	void injectLife(int positions[], int size);

	void injectLife(int position);

	// Test
	void addOccilator (int pos);
	void addStill(int pos);
	void randomise();
};

