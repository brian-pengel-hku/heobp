#include <time.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include "Grid.h"

/*
 * In plaats van alle cellen af te lopen op elke itteratie
 * loop ik alleen de actieve en mogelijk beinvloede cellen af.
 * 
 */

#ifdef _WIN32
	#include <windows.h>

	void sleep(unsigned milliseconds) {
		Sleep(milliseconds);
	}
#else
	#include <unistd.h>

	void sleep(unsigned milliseconds) {
		usleep(milliseconds * 1000);
	}
#endif

void clearScreen() {
	/*int n;
	for(n = 0; n < 10; n++)
		printf("\n\n\n\n\n\n\n\n\n\n");*/

	if(system("CLS")) system("clear");
}

int askInput(std::string text) {
	int i;
	std::cout << "\n\n" << text ;
	std::cin >> i;
	return i;
}

int main() {
	std::cout << "In plaats van alle cellen af te lopen op elke itteratie loop ik alleen de actieve en mogelijk beinvloede cellen af.\nDit om mijzelf iets meer uit te dagen. \n\n";

	int fieldHeight = (fieldHeight = askInput("How tall should the grid be (min 3): "), fieldHeight < 3 ? 3 : fieldHeight),
		cnt = askInput("How many itterations would you like to see: "),
		delay = (delay = askInput("On how large a delay: "), delay < 1 ? 1 : delay) ;

	if(cnt <= 0) 
		return 0;

	Grid * grid = new Grid(100, 25);
//	Grid * grid = new Grid(fieldHeight * 2, fieldHeight);
	//grid->addOccilator(3);
	//grid->addStill(12);
	grid->randomise();

	while(cnt > 0) {
		time_t start = time(0);

		clearScreen();
		std::cout << "Grid (" << fieldHeight*2 << ", " << fieldHeight << ")\n" ;
		std::cout << "Current active population: " << grid->getActivePopulation() << std::endl;
		std::cout << "Itterations left: " << cnt << std::endl;

		grid->ClearField();
		grid->liveGeneration();
		grid->CreateFieldRows();
		grid->DrawField();


		sleep(start + delay - time(0));

		cnt--;
		if(cnt <= 0)
			cnt = askInput("How many itterations would you like to see (0 will end the program and properly garbage collect): ");
	}

	delete grid;
	std::cin.get();
	
	return 0;
}