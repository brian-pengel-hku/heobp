#include "Cell.h"

Cell::Cell(Grid* grid, int position) {
	this->grid = grid;
	this->position = position;

	this->x = position % grid->getWidth();
	this->y = (position - this->x) / grid->getWidth();
}


Cell::~Cell() {
	//std::cout << "destroyed cell: " + std::to_string(position) << std::endl;
}

bool Cell::isAlive() {
	return this->alive;
}

void Cell::setAlive(bool alive) {
	this->alive = alive;
}

void Cell::reset() {
	activeNeighbours = 0;
}

int Cell::getXPosition() {
	return this->x;
}

int Cell::getYPosition() {
	return this->y;
}

int Cell::getPosition() {
	return this->position;
}

void Cell::saveNeighbours () {
	if(grid->isCell(x - 1, y - 1)) neighbours.push_back(grid->getCell(x - 1, y - 1));
	if(grid->isCell(x    , y - 1)) neighbours.push_back(grid->getCell(x    , y - 1));
	if(grid->isCell(x + 1, y - 1)) neighbours.push_back(grid->getCell(x + 1, y - 1));

	if(grid->isCell(x - 1, y)) neighbours.push_back(grid->getCell(x - 1, y));
	if(grid->isCell(x + 1, y)) neighbours.push_back(grid->getCell(x + 1, y));

	if(grid->isCell(x - 1, y + 1)) neighbours.push_back(grid->getCell(x - 1, y + 1));
	if(grid->isCell(x    , y + 1)) neighbours.push_back(grid->getCell(x    , y + 1));
	if(grid->isCell(x + 1, y + 1)) neighbours.push_back(grid->getCell(x + 1, y + 1));
}


void Cell::live() {
	if(neighbours.empty())
		saveNeighbours();
	activeNeighbours = 0;

	auto i = neighbours.begin();
	while(i != neighbours.end()) {
		if((*i)->isAlive())
			activeNeighbours++;
		else (*i)->knock();
		i++;
	}
}

std::string Cell::getLiveNeighbourCount() {
	return std::to_string(activeNeighbours);
}

void Cell::knock() {
	if(activeNeighbours++, activeNeighbours == 1)
		grid->addPossibleActiveCell(this);
}

bool Cell::shouldBeAlive() {
	alive = ((alive && activeNeighbours == 2) || activeNeighbours == 3);
	return alive;
}