#include <iostream>
#include <string>
#include "Queue.h"

template <typename T, size_t size>
void printArray(T (&arr)[size]) {
	
	for(int i = 0; i < size; ++i)
		std::cout << arr[i] << ", ";

	std::cout << std::endl;
}


template <typename T, size_t size>
void sortArray(T (&arr)[size], bool compare (T, T)) {
	for(size_t i = 1; i < size; ++i) {
		T curr = arr[i];
		int j = i - 1;

		while(j > -1 && compare(curr, arr[j])) {
			arr[j + 1] = arr[j];
			arr[j] = curr;
			j--;
		}
	}
}

bool largerThan (float a, float b) {
	return a > b;
}

bool longerThan (std::string a, std::string b) {
	return a.length() > b.length();
}

int main () {
	// Array
	std::cout << "\n--- Array challenge output:\n\n";

	float testArr[] = {5,4, 5,6,4,9,3, 32 ,6};
	printArray<float>(testArr);
	sortArray<float>(testArr, largerThan);
	printArray<float>(testArr);

	std::string strArr[] = {"Hi", "How are you?", "That's great", "me?", "I'm doing very well"};
	printArray<std::string>(strArr);
	sortArray<std::string>(strArr, longerThan);
	printArray<std::string>(strArr);

	// Queue
	std::cout << "\n--- Queue challenge output:\n\n";

	Queue<int> * q = new Queue<int>();
	q->put(14);
	q->put(0);
	q->put(42);
	q->put(12);

	std::cout << "Queue size: " << q->size() << std::endl;

	size_t qSize = q->size();
	for(size_t cnt = 0; cnt < qSize; cnt++) {
		std::cout << "Get value: " << q->get();
		std::cout << " | new size: " << q->size() << std::endl;
	}

	q->put(14);
	delete q;
	std::cin.get();

	return 0;
}