#pragma once

template <typename T>
class Queue {
	private:
	struct Node {

		Node (T val) { 
			this->val = val; 
			next = nullptr;
		}

		public:
		T val;
		Node * next;
	};

	public:
	Node * head;
	Node * tail;

	Queue();
	~Queue();

	void put(T val);
	T get();
	size_t size();
};


template <typename T>
Queue<T>::Queue() {
	head = tail = nullptr;
}

template <typename T>
Queue<T>::~Queue() {
	while(get() != NULL);
}

template<typename T>
void Queue<T>::put(T val) {
	Node * newNode = new Node (val);
	if(tail == NULL)
		head = tail = newNode;
	else {
		tail->next = newNode;
		tail = newNode;
	}
}

template<typename T>
T Queue<T>::get() {
	if(head == nullptr)
		return (T)nullptr;

	Node * res = head;
	T resVal = head->val;
	head = head->next;
	delete res;

	if(tail == res)
		tail = NULL;

	return resVal;
}

template<typename T>
size_t Queue<T>::size() {
	if(head == nullptr)
		return 0;

	size_t cnt = 1;
	Node * cur = head;
	while((cur = cur->next) != nullptr)
		cnt++;

	return cnt;
}